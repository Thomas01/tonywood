Please following steps will help to run the project

// PROJECT SPECS
TECHNOLOGY USED
Laravel 8
Mysql
Vuejs, js , css , boostrap 

UI
Dashbaord
System management
Employee management => used Vuejs for frontend
Users Management 

FUNCTIONALITY
CRUD
Aunthentication
Password reset


install or setup web server (I use xampp which includes php and mysql)

install node js
rum composer install
run composer update
run npm install

configure your .env file accorded in your as bellow
    DB_CONNECTION=mysql // DEFAULT DB_HOST=127.0.0.1 // DEFAULT DB_PORT=3306 // DEFAULT
    DB_DATABASE=YOUR_DB_NAME 
    DB_USERNAME=YOUR_DB_USERNAME 
    DB_PASSWORD=YOUR_DB_PASSWORD

Run this command to clear your config
     php artisan config:clear

Run migration for tables.
    php artisan migrate

Please run below command to start project
    php artisan serve

To begin, Register a user
    Click on register on top right navbar
    Password MUST be atleast 8 characters 
    App should redirect to Dashboard
    Click on Users to Manage users
    Click on any ui
    To LogOut click Username on top right